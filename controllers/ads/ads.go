// Copyright 2018 Ug Ltd
//           2018 Kirill Zhuharev
package ads

import (
	"log"

	"ug/models"
	"ug/pkg/context"
	"ug/pkg/upload"

	"github.com/golang/protobuf/ptypes"
	"github.com/rs/xid"
)

func Create(c *context.Context, af models.AdForm, adData models.OfferData, comfort models.Comfort) {
	if c.Req.Method == "POST" {
		ids := models.CachedIDs(af.Xid)
		var urls []string
		for _, id := range ids {
			urls = append(urls, upload.ImageURL(id))
		}
		models.DropCache(af.Xid)

		adData.Images = urls
		adData.Price = af.Price

		adData.Comfort = &comfort

		ad := models.Offer{
			Date:          ptypes.TimestampNow(),
			Text:          af.Text,
			OwnerId:       c.User.GetId(),
			Published:     false,
			PublishedDate: ptypes.TimestampNow(),
			Data:          &adData,
			State:         models.Offer_Moderation,
		}
		err := models.Offers.Create(&ad)
		if err != nil {
			log.Printf("err create ads: %s", err)
			c.Error(200, err.Error())
			return
		}

		models.TmpIndex()

		c.Redirect("/filter")
		return
	}

	c.Data["pageXid"] = xid.New()
	c.HTML(200, "ads/create")
}

func List(c *context.Context) {
	ads, err := models.Offers.List()
	if err != nil {
		return
	}
	c.Data["ads"] = ads
	users, err := models.Users.ForAds(ads)
	if err != nil {
		return
	}
	c.Data["users"] = users
	c.HTML(200, "ads/list")
}

func Publish(ctx *context.Context) {
	err := models.Offers.Publish(ctx.ParamsInt64(":id"))
	if err != nil {
		log.Println(err)
	}
	ctx.Redirect("/lk/moderation")
}

func Reject(ctx *context.Context) {
	err := models.Offers.Reject(ctx.ParamsInt64(":id"))
	if err != nil {
		log.Println(err)
	}
	ctx.Redirect("/lk/moderation")
}

func Show(ctx *context.Context) {
	ad, _ := models.Offers.ByID(ctx.ParamsInt64(":id"))
	ctx.Data["ad"] = ad
	ctx.HTML(200, "ads/show")
}
