// Copyright 2018 Uteka Ltd
//           2018 Kirill Zhuharev

package ads

import (
	"log"

	"ug/models"
	"ug/pkg/context"
)

func Rejected(ctx *context.Context) {
	ads, err := models.Offers.Rejected()
	if err != nil {
		log.Printf("err get rejected: %s", err)
		return
	}
	log.Printf("found %d ads", len(ads))
	ctx.Data["ads"] = ads
	ctx.HTML(200, "lk/rejected")
}

func Approved(ctx *context.Context) {
	ads, err := models.Offers.List()
	if err != nil {
		log.Printf("err get rejected: %s", err)
		return
	}
	log.Printf("found %d ads", len(ads))
	ctx.Data["ads"] = ads
	ctx.HTML(200, "lk/approved")
}
