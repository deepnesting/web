// Copyright 2018 Ug Ltd
//           2018 Kirill Zhuharev

package auth

import (
	"fmt"
	"log"
	"net/url"
	"strconv"

	"ug/models"
	"ug/pkg/base"
	"ug/pkg/context"

	"github.com/zhuharev/vkutil"
)

const (
	userKey = "u"
)

func authorize(ctx *context.Context, userID int64) error {
	err := ctx.Session.Set(userKey, userID)
	if err != nil {
		return err
	}
	return nil
}

func DevLogin(c *context.Context) {
	id := c.QueryInt64("id")
	authorize(c, id)
	c.Redirect("/users/" + strconv.Itoa(int(id)))
}

func Reg(c *context.Context) {
	c.HTML(200, "auth/reg")
}

func Logout(c *context.Context) {
	c.Session.Delete(userKey)
	c.SetCookie("s", "")
	c.Redirect("/reg")
}

func Vk(c *context.Context) {
	c.Redirect("https://oauth.vk.com/authorize?client_id=6355667&redirect_uri=https://ugnest.com/auth/vk/callback&response_type=code")
}

func VkCallback(c *context.Context) {
	code := c.Query("code")

	frmt := "https://oauth.vk.com/access_token?client_id=%d&client_secret=%s&redirect_uri=https://ugnest.com/auth/vk/callback&code=%s"

	var AT struct {
		AccessToken string `json:"access_token"`
		UserID      int64  `json:"user_id"`
	}
	u := fmt.Sprintf(frmt, 6355667, "uND01iGbJZLNznbMWE10", code)
	err := base.FileUnmarshallJSON(u, &AT)
	if err != nil {
		log.Printf("get access token err=%s", err)
		c.Error(500, err.Error())
		return
	}

	vkUtil := vkutil.NewWithToken(AT.AccessToken)
	vkUtil.VkApi.Lang = "ru"
	users, err := vkUtil.UsersGet(AT.UserID, url.Values{"fields": {"photo"}})
	if err != nil {
		log.Printf("vk users.get user_id=%d err=%s", AT.UserID, err)
		c.Error(500, err.Error())
		return
	}
	if len(users) != 1 {
		log.Printf("vk users.get user_id=%d err=%s", AT.UserID, "unexpected slice len")
		c.Error(500, err.Error())
		return
	}

	vkUser := users[0]

	user, err := models.Users.CreateByVkIfNotExists(AT.UserID, vkUser.FirstName, vkUser.LastName, vkUser.Photo)
	if err != nil {
		log.Printf("create user by vk_id=%d: %s", AT.UserID, err)
		c.Error(500, err.Error())
		return
	}

	err = authorize(c, user.Id)
	if err != nil {
		log.Printf("err set session user_id=%d: %s", user.Id, err)
		c.Error(500, err.Error())
		return
	}

	log.Println(AT.AccessToken)
	c.Redirect("/users/" + strconv.Itoa(int(user.Id)))
}
