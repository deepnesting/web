// Copyright 2018 Ujutnoe gnjozdyshko
//           2018 Kirill Zhuharev

package auth

import (
	"fmt"
	"log"

	"ug/models"
	"ug/pkg/context"
	"ug/pkg/smsubmit"
)

func PhoneReg(ctx *context.Context) {
	if ctx.Req.Method == "POST" {
		smsubmit.SendCode(ctx.Query("phone"))
		ctx.Redirect("/auth/sms/submit?phone=" + ctx.Query("phone"))
		return
	}
	ctx.HTML(200, "auth/reg-with-phone")
}

func Submit(ctx *context.Context) {
	if ctx.Req.Method == "POST" {
		if ok, _ := smsubmit.CheckCode("session", ctx.Query("code")); ok {
			phone := ctx.Query("phone")
			user, err := models.Users.MustByPhone(phone)
			if err != nil {
				log.Printf("err must by phone %s", err)
				ctx.Redirect("/")
				return
			}
			authorize(ctx, user.Id)
			ctx.Redirect("/users/" + fmt.Sprint(user.Id))
			return
		} else {
			log.Printf("err check sms code")
		}
	}
	ctx.Data["phone"] = ctx.Query("phone")
	ctx.HTML(200, "auth/submit")
}
