// Copyright 2018 Kirill Zhuharev. All rights reserved.
// Use of this source code is governed by a MIT-style
// license that can be found in the LICENSE file.

package auth

import (
	"ug/pkg/context"
)

// MustSigned проверяет, вошёл ли пользователь в аккаунт, если нет возвращает
// 403 ошибку
func MustSigned(ctx *context.Context) {
	if !ctx.IsSigned {
		ctx.Error(403, "Доступ запрещён")
	}
}
