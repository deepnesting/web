// Copyright 2018 Ujutnoe gnjozdyshko
//           2018 Kirill Zhuharev

package billing

import (
	"log"
	"ug/models"
	"ug/pkg/context"

	"github.com/golang/protobuf/ptypes"
)

// secret oxrupDWGzh2F7Bg4S70N0Jnf

func Refill(ctx *context.Context) {
	account, err := models.Accounts.Get(ctx.User.Id)
	if err != nil {
		log.Println(err)
		return
	}
	ctx.Data["account"] = account
	ctx.HTML(200, "billing/refill")
}

func YaCallback(ctx *context.Context) int {
	var (
		testNotification = ctx.QueryBool("test_notification")
		unaccepted       = ctx.QueryBool("unaccepted")
		amount           = ctx.QueryFloat64("amount")
		withdrawAmount   = ctx.QueryFloat64("withdraw_amount")
		label            = ctx.Query("label")
	)
	log.Printf("amount: %v (%v) label: %v, test: %v, unacepted: %v", amount, withdrawAmount, label, testNotification, unaccepted)

	if unaccepted {
		log.Printf("unaceppted txn, skip")
		return 200
	}
	if testNotification {
		log.Printf("test txn, skip")
		return 200
	}
	var (
		userID = ctx.QueryInt64("label")
	)

	account, err := models.Accounts.Get(userID)
	if err != nil {
		log.Printf("err get account for user=%d", userID)
		return 200
	}

	txn := &models.Transaction{
		PaymentSystemId: 1, //todo
		Amount:          uint32(withdrawAmount),
		Status:          models.Transaction_Done,
		AccountId:       account.UserId,
		CreatedAt:       ptypes.TimestampNow(),
	}

	err = models.Accounts.CreateTransaction(txn)
	if err != nil {
		log.Printf("err creating txn: %v", err)
		return 200
	}

	_, err = models.Accounts.IncBalance(userID, txn.Amount)
	if err != nil {
		log.Printf("err inc balance: %v", err)
		return 200
	}

	return 200
}

// Id              int64              `protobuf:"varint,1,opt,name=id" json:"id,omitempty"`
// PaymentSystemId int64              `protobuf:"varint,2,opt,name=payment_system_id,json=paymentSystemId" json:"payment_system_id,omitempty"`
// Amount          uint32             `protobuf:"varint,3,opt,name=amount" json:"amount,omitempty"`
// Status          Transaction_Status `protobuf:"varint,4,opt,name=status,enum=models.Transaction_Status" json:"status,omitempty"`
// AccountId       int64              `protobuf:"varint,5,opt,name=account_id,json=accountId" json:"account_id,omitempty"`
// // @inject_tag: xorm:"jsonb notnull default '{}' 'created_at'"
// CreatedAt  *google_protobuf1.Timestamp `protobuf:"bytes,6,opt,name=created_at,json=createdAt" json:"created_at,omitempty" xorm:"jsonb notnull default '{}' 'created_at'"`
// Comment    string                      `protobuf:"bytes,7,opt,name=comment" json:"comment,omitempty"`
// ExternalId string
