package comments

import (
	"ug/models"
	"ug/pkg/context"
)

// Add add like to ad
func Add(ctx *context.Context) {
	next := ctx.Query("next")
	id := ctx.QueryInt64(":id")

	models.Comments.Create(models.PeerType_offer, id, ctx.Query("text"))

	ctx.Redirect(next)
}
