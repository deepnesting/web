package likes

import (
	"ug/models"
	"ug/pkg/context"
)

// Add or remove like to ad
func Add(ctx *context.Context) {
	adID := ctx.ParamsInt64(":id")
	models.Reactions.Toggle(models.PeerType_offer, adID, ctx.User.Id, models.ReactionType_Like)
	ctx.JSON(200, "ok")
}

// Dislike set dislike to post
func Dislike(ctx *context.Context) {
	adID := ctx.ParamsInt64(":id")

	models.Reactions.Toggle(models.PeerType_offer, adID, ctx.User.Id, models.ReactionType_Dislike)
	ctx.JSON(200, "ok")
}
