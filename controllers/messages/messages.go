package messages

import (
	"fmt"
	"log"
	"ug/models"
	"ug/pkg/context"
)

func Dialogs(ctx *context.Context) {
	dialogs, err := models.Messages.Dialogs(ctx.User.Id)
	if err != nil {
		log.Println(err)
	}
	ctx.Data["dialogs"] = dialogs
	ctx.HTML(200, "messages/dialogs")
}

func History(ctx *context.Context) {
	var peer = ctx.ParamsInt64("peer")
	messages, err := models.Messages.History(ctx.User.Id, peer)
	if err != nil {
		log.Println(err)
	}
	ctx.Data["messages"] = messages
	ctx.HTML(200, "messages/history")
}

func Send(ctx *context.Context) {
	var (
		text = ctx.Query("text")
		peer = ctx.ParamsInt64("peer")
	)
	_, err := models.Messages.Create(ctx.User.Id, peer, text)
	if err != nil {
		log.Println(err)
	}
	ctx.Redirect("/im/" + fmt.Sprint(peer))
}
