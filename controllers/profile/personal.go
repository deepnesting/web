// Copyright 2018 Kirill Zhuharev. All rights reserved.
// Use of this source code is governed by a MIT-style
// license that can be found in the LICENSE file.

package profile

import (
	"log"
	"ug/models"
	"ug/pkg/context"
)

// Personal update personal user info
func Personal(ctx *context.Context) {
	if ctx.Req.Method == "POST" {
		firstName := ctx.Query("first_name")
		lastName := ctx.Query("last_name")
		ctx.User.Data.FirstName = firstName
		ctx.User.Data.LastName = lastName
		err := models.Users.Update(ctx.User)
		if err != nil {
			log.Printf("update user err=%s", err)
		}
	}
	ctx.HTML(200, "profile/personal")
}
