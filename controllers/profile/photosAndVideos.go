// Copyright 2018 Uteka Ltd
//           2018 Kirill Zhuharev

package profile

import (
	"ug/pkg/context"
)

// PhotosAndVideos update personal user info
func PhotosAndVideos(ctx *context.Context) {

	ctx.HTML(200, "profile/photos-and-videos")
}
