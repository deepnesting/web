// Copyright 2018 Uteka Ltd
//           2018 Kirill Zhuharev

package search

import (
	"log"
	"ug/models"
	"ug/pkg/context"
)

func Facet(ctx *context.Context) {
	ads, err := models.Search(ctx.Req.URL.Query())
	if err != nil {
		return
	}
	log.Printf("found %d ads", len(ads))
	ctx.Data["ads"] = ads
	users, err := models.Users.ForAds(ads)
	if err != nil {
		return
	}

	ctx.Data["query"] = ctx.Req.URL.Query()
	ctx.Data["users"] = users
	ctx.HTML(200, "ads/list")
}
