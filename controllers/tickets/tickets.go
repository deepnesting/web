// Copyright 2018 Ujutnoe gnjozdyshko
//           2018 Kirill Zhuharev

package tickets

import (
	"ug/pkg/context"
)

// Add create ticket
func Add(ctx *context.Context) {

	ctx.Redirect("/")
}
