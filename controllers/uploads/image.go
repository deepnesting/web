// Copyright 2018 Ug Ltd
//           2018 Kirill Zhuharev
package uploads

import (
	"bytes"
	"fmt"
	"image"
	_ "image/gif"
	"image/jpeg"
	_ "image/png"

	"ug/models"
	"ug/pkg/context"
	"ug/pkg/upload"

	"github.com/fatih/color"
	_ "golang.org/x/image/bmp"
	_ "golang.org/x/image/tiff"
)

func UploadImage(c *context.Context) {
	var (
		err string
	)

	c.Req.ParseMultipartForm(32 << 20)
	file, _, e := c.Req.FormFile("file")
	if e != nil {
		fmt.Println(e)
		return
	}
	defer file.Close()
	//fmt.Fprintf(w, "%v", handler.Header)

	img, _, e := image.Decode(file)
	if e != nil {
		color.Red("%s", e)
		return
	}

	buf := bytes.NewBuffer(nil)
	e = jpeg.Encode(buf, img, &jpeg.Options{Quality: 85})
	if e != nil {
		color.Red("%s", e)
		return
	}

	id, e := models.Put(buf.Bytes())
	if e != nil {
		fmt.Println(e)
		err = e.Error()
	}

	models.CacheID(c.Query("xid"), id)

	c.JSON(200, map[string]interface{}{
		"success": fmt.Sprint(id),
		"error":   err,
		"err":     e,
		"photo":   upload.ImageURL(id),
	})
}
