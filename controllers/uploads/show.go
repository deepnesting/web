// Copyright 2018 Ug Ltd
//           2018 Kirill Zhuharev
package uploads

import (
	"strings"

	"ug/models"
	"ug/pkg/context"

	"github.com/Unknwon/com"
)

func Show(c *context.Context) {
	strId := c.Params(":id")
	strId = strings.TrimSuffix(strId, ".jpg")
	id := com.StrTo(strId).MustInt64()
	//fmt.Println(id)
	bts, e := models.Get(id)
	if e != nil {
		panic(e)
	}
	//fmt.Println(len(bts))
	c.Resp.Header().Set("Content-Type", "image/jpeg")
	c.Resp.WriteHeader(200)
	c.Resp.Write(bts)
}
