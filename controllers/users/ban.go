// Copyright 2018 Ug Ltd
//           2018 Kirill Zhuharev
package users

import (
	"log"
	"ug/models"
	"ug/pkg/context"
)

func Ban(ctx *context.Context) {
	err := models.Users.Ban(ctx.ParamsInt64(":id"))
	if err != nil {
		log.Printf("ban err: %s", err)

	}
	ctx.Redirect("/lk/users")
}

func UnBan(ctx *context.Context) {
	err := models.Users.UnBan(ctx.ParamsInt64(":id"))
	if err != nil {
		log.Printf("unban err: %s", err)

	}
	ctx.Redirect("/lk/users")
}
