// Copyright 2018 Ug Ltd
//           2018 Kirill Zhuharev
package users

import (
	"log"

	"ug/models"
	"ug/pkg/context"
)

func Get(ctx *context.Context) {
	id := ctx.ParamsInt64(":id")
	if id == 0 {
		id = ctx.User.Id
	}
	user, err := models.Users.ByID(id)
	if err != nil {
		log.Printf("get user user=%d err=%s", id, err)
		ctx.Error(500, err.Error())
		return
	}
	ctx.Data["user"] = user
	ctx.HTML(200, "users/get")
}

func Edit(ctx *context.Context) {
	id := ctx.ParamsInt64(":id")
	if id == 0 {
		id = ctx.User.Id
	}
	user, err := models.Users.ByID(id)
	if err != nil {
		log.Printf("get user user=%d err=%s", id, err)
		ctx.Error(500, err.Error())
		return
	}
	ctx.Data["user"] = user
	ctx.HTML(200, "users/edit")
}

func List(ctx *context.Context) {
	users, err := models.Users.List()
	if err != nil {
		log.Printf("get user user list err=%s", err)
		ctx.Error(500, err.Error())
		return
	}
	ctx.Data["users"] = users
	ctx.HTML(200, "lk/users")
}

func Banned(ctx *context.Context) {
	users, err := models.Users.Banned()
	if err != nil {
		log.Printf("get user user list err=%s", err)
		ctx.Error(500, err.Error())
		return
	}
	ctx.Data["users"] = users
	ctx.HTML(200, "lk/banned")
}
