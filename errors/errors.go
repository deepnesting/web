// Copyright 2018 Ug Ltd
//           2018 Kirill Zhuharev

package errors

import "strconv"

type Error int

func (e Error) Error() string {
	return strconv.Itoa(int(e))
}

const (
	NotFound Error = iota + 1
	AlreadyExists
)
