// Copyright 2018 Ujutnoe gnjozdyshko
//           2018 Kirill Zhuharev

package models

import (
	"fmt"

	"github.com/golang/protobuf/ptypes"
)

type accountModel int

var Accounts accountModel

func (accountModel) Get(userID int64) (*Account, error) {
	account := new(Account)
	has, err := db.Where("user_id = ?", userID).Get(account)
	if err != nil {
		return nil, err
	}
	if !has {
		account = new(Account)
		account.UserId = userID
		account.Balance = 0
		_, err = db.Insert(account)
		if err != nil {
			return nil, err
		}
	}
	return account, nil
}

func (accountModel) History(userID int64) (res []Transaction, err error) {
	err = db.Where("account_id = ?", userID).OrderBy("id desc").Find(&res)
	return
}

func (am accountModel) IncBalance(userID int64, amount uint32) (uint32, error) {
	_, err := db.Exec("update account set balance = balance + ? where user_id = ?", amount, userID)
	if err != nil {
		return 0, err
	}
	account, err := am.Get(userID)
	if err != nil {
		return 0, err
	}
	return account.Balance, nil
}

func (accountModel) CreateTransaction(txn *Transaction) error {
	txn.CreatedAt = ptypes.TimestampNow()
	_, err := db.Insert(txn)
	if err != nil {
		return err
	}
	return nil
}

func (accountModel) UpdateTransactionStatus(txnID int64, newStatus Transaction_Status) error {
	_, err := db.Exec("update transaction set status = ? where id = ?", newStatus, txnID)
	if err != nil {
		return err
	}
	return nil
}

func (am accountModel) ByTxnID(txnID int64) (*Account, error) {
	txn := new(Transaction)
	has, err := db.Id(txnID).Get(txn)
	if err != nil {
		return nil, err
	}
	if !has {
		return nil, fmt.Errorf("txn not a found")
	}
	return am.Get(txn.AccountId)
}
