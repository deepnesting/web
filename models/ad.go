// Copyright 2018 Ug Ltd
//           2018 Kirill Zhuharev

package models

type OfferSlice []Offer

func userIDsFromAds(ads []Offer) (res []int64) {
	var dup = map[int64]bool{}
	for _, ad := range ads {
		dup[ad.OwnerId] = true
	}
	for id := range dup {
		res = append(res, id)
	}
	return
}

type AdForm struct {
	Xid    string   `form:"page_xid"`
	Price  uint32   `form:"price"`
	Text   string   `form:"text"`
	Owner  int64    `form:"owner"`
	Images []string `form:"images"`
}

func (ad *Offer) Type() string {
	return "ad"
}
