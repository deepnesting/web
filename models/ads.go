// Copyright 2018 Ug Ltd
//           2018 Kirill Zhuharev

package models

import (
	"ug/errors"

	"github.com/golang/protobuf/ptypes"
)

type adModel int

// Offers /ads api endpoint, lol
var Offers adModel

func (adModel) ByID(id int64) (*Offer, error) {
	ad := new(Offer)
	has, err := db.Id(id).Get(ad)
	if err != nil {
		return nil, err
	}
	if !has {
		return nil, errors.NotFound
	}
	return ad, nil
}

func (adModel) Create(ad *Offer) error {
	ad.Published = false
	ad.Date = ptypes.TimestampNow()
	//ad.PublishedDate = ptypes.TimestampNow()
	_, err := db.InsertOne(ad)
	if err != nil {
		return err
	}
	return nil
}

func (am adModel) Publish(adID int64) error {
	ad, err := am.ByID(adID)
	if err != nil {
		return err
	}
	ad.Published = true
	ad.PublishedDate = ptypes.TimestampNow()
	ad.State = Offer_Approved
	_, err = db.Id(adID).Cols("published", "published_date", "state").Update(ad)
	TmpIndex()
	return err
}

func (am adModel) Reject(adID int64) error {
	ad, err := am.ByID(adID)
	if err != nil {
		return err
	}
	ad.Published = false
	ad.PublishedDate = ptypes.TimestampNow()
	ad.State = Offer_Rejected
	_, err = db.Id(adID).Cols("published", "published_date", "state").Update(ad)
	TmpIndex()
	return err
}

func (adModel) CreateFromForm(af AdForm) (*Offer, error) {
	ad := Offer{
		Date:          ptypes.TimestampNow(),
		Text:          af.Text,
		OwnerId:       af.Owner,
		Published:     true,
		PublishedDate: ptypes.TimestampNow(),
		Data: &OfferData{
			Images: af.Images,
			Price:  af.Price,
		},
	}
	_, err := db.InsertOne(&ad)
	if err != nil {
		return nil, err
	}

	return &ad, nil
}

// func (adModel) Create(text string, owner int64, images []string) (*Offer, error) {
// 	ad := Offer{
// 		Date:          ptypes.TimestampNow(),
// 		Text:          text,
// 		OwnerId:       owner,
// 		Published:     true,
// 		PublishedDate: ptypes.TimestampNow(),
// 		Data: &OfferData{
// 			Images: images,
// 		},
// 	}
//
// 	_, err := db.InsertOne(&ad)
// 	if err != nil {
// 		return nil, err
// 	}
//
// 	return &ad, nil
// }

func (adModel) List() (res []Offer, err error) {
	err = db.Where("state = ?", Offer_Approved).OrderBy("id desc").Find(&res)
	if err != nil {
		return
	}
	return
}

func (adModel) ByIDs(ids []int64) (res []Offer, err error) {
	err = db.In("id", ids).OrderBy("id desc").Find(&res)
	return
}

func (adModel) OnModeration() (res []Offer, err error) {
	err = db.Where("state = ? or state = ?", Offer_Moderation, Offer_Unknown).OrderBy("id desc").Find(&res)
	return
}

func (adModel) Rejected() (res []Offer, err error) {
	err = db.Where("state != ? and state != ?", Offer_Moderation, Offer_Approved).OrderBy("id desc").Find(&res)
	return
}
