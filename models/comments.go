package models

type commentModel int

// Comments endpoint for comments funcional
var Comments commentModel

func (commentModel) ByObjectID(objectType PeerType, objectID int64) (res []Comment, err error) {
	err = db.Where("object_type = ? AND object_id = ?", objectType, objectID).OrderBy("id desc").Find(&res)
	return
}

func (commentModel) Create(objectType PeerType, objectID int64, text string) (int64, error) {
	comment := &Comment{
		ObjectType: objectType,
		ObjectId:   objectID,
		Text:       text,
	}
	return db.InsertOne(comment)
}
