package models

type reactionModel int

// Reactions public endpoint with likes iteration
var Reactions reactionModel

func (reactionModel) Toggle(objType PeerType, objID int64, likerID int64, reaction ReactionType) error {
	_, err := db.Exec(`INSERT INTO reaction (object_type, object_id, liker_id, reaction_type)
  VALES (?, ?, ?, ?)
  ON CONFLICT DO UPDATE SET reaction_type = ?`, objType, objID, likerID, reaction, reaction)
	return err
}

func (reactionModel) Reactions() {

}

// ObjectReaction will be embed in template view
type ObjectReaction struct {
	Reaction ReactionType
	Count    int
}
