package models

import "fmt"
import "github.com/golang/protobuf/ptypes"

type msgModel int

// Messages endpoint for messaging
var Messages msgModel

func (mm msgModel) Create(from, to int64, text string) (int64, error) {
	msg := &Message{
		ChatId: chatKey(from, to),
		Sender: from,
		Text:   text,
		Date:   ptypes.TimestampNow(),
	}
	_, err := db.InsertOne(msg)
	if err != nil {
		return 0, err
	}

	err = mm.SetReaded(from, to, from, msg.Id)
	if err != nil {
		return 0, err
	}

	return msg.Id, err
}

func chatKey(peer1, peer2 int64) string {
	if peer1 > peer2 {
		peer1, peer2 = peer2, peer1
	}
	return fmt.Sprintf("%d:%d", peer1, peer2)
}

func (msgModel) History(peer1, peer2 int64) (res []Message, err error) {
	err = db.Where("chat_id = ?", chatKey(peer1, peer2)).OrderBy("id desc").Find(&res)
	return
}

func (msgModel) Dialogs(peer int64) (res []Dialog, err error) {
	err = db.Where("peer1 = ? or peer2 = ?", peer, peer).OrderBy("last_message_id desc").Find(&res)
	return
}

func (msgModel) SetReaded(peer1, peer2, initiator, lmID int64) error {
	chatID := chatKey(peer1, peer2)
	sql := `INSERT INTO dialog (chat_id, last_message_id,peer1,peer2) VALUES (?,?,?,?)
ON CONFLICT (chat_id) DO UPDATE SET last_message_id = ?;`
	if peer1 != initiator {
		sql = `INSERT INTO dialog (chat_id, last_message_id) VALUES (?,?)
ON CONFLICT (chat_id) DO UPDATE SET last_message_id = ?, readed = ?;`
		_, err := db.Exec(sql, chatID, lmID, lmID, true)
		if err != nil {
			return err
		}
		return nil
	}

	if peer1 > peer2 {
		peer1, peer2 = peer2, peer1
	}
	_, err := db.Exec(sql, chatID, lmID, peer1, peer2, lmID)
	return err
}

func (d Dialog) Peer(curPeer int64) int64 {
	if d.Peer1 == curPeer {
		return d.Peer2
	}
	return d.Peer1
}
