// Copyright 2018 Ujutnoe gnjozdyshko
//           2018 Kirill Zhuharev

package models

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
)

type Metro struct {
	Lines []struct {
		HexColor string         `json:"hex_color"`
		Stations []MetroStation `json:"stations"`
		ID       string         `json:"id"`
		Name     string         `json:"name"`
	} `json:"lines"`
	ID   string `json:"id"`
	Name string `json:"name"`
}

type MetroStation struct {
	Lat   float64 `json:"lat"`
	Lng   float64 `json:"lng"`
	Order int     `json:"order"`
	ID    string  `json:"id"`
	Name  string  `json:"name"`
}

var msk Metro
var spb Metro

func init() {
	data, err := ioutil.ReadFile("./data/metro/moscow.json")
	if err != nil {
		panic(fmt.Sprintf("err reading moscow json file: %s", err))
	}

	err = json.Unmarshal(data, &msk)
	if err != nil {
		panic(fmt.Sprintf("err unmarshal json: %s", err))
	}

	data, err = ioutil.ReadFile("./data/metro/spb.json")
	if err != nil {
		panic(fmt.Sprintf("err reading spb json file: %s", err))
	}

	err = json.Unmarshal(data, &spb)
	if err != nil {
		panic(fmt.Sprintf("err unmarshal json: %s", err))
	}
}

func (m Metro) Get(id string) (*MetroStation, error) {
	for _, line := range m.Lines {
		for _, c := range line.Stations {
			if c.ID == id {
				return &c, nil
			}
		}
	}
	return nil, nil
}

type metroApi int

var Metros metroApi

func (metroApi) Get(city int, id string) (*MetroStation, error) {
	switch city {
	case 1:
		return msk.Get(id)
	default:
		return spb.Get(id)
	}
	return nil, nil
}

func (metroApi) All(city int) (*Metro, error) {
	switch city {
	case 1:
		return &msk, nil
	default:
		return &spb, nil
	}
	return nil, nil
}
