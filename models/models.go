// Copyright 2018 Ug Ltd
//           2018 Kirill Zhuharev
package models

import (
	"log"
	"net/url"
	"path/filepath"
	"strconv"

	"ug/errors"
	"ug/pkg/search"
	"ug/pkg/setting"

	"github.com/blevesearch/bleve"
	"github.com/blevesearch/bleve/search/query"
	"github.com/go-xorm/xorm"
	"github.com/zhuharev/bloblog"
)

var (
	db *xorm.Engine

	blog *bloblog.BlobLog
)

func NewContext(dev bool) {
	var err error
	dsn := "postgres://postgres:30b62d5aebf9086c74c9a84cf8c0c7a9@dokku-postgres-ugdb:5432/ugdb?sslmode=disable"
	if dev {
		dsn = "postgres://postgres@127.0.0.1:5432/ugdb?sslmode=disable"
	}
	db, err = xorm.NewEngine("postgres", dsn)
	if err != nil {
		panic("failed to connect database: " + err.Error())
	}

	db.ShowSQL()

	err = db.Sync2(&User{},
		&Socials{},
		&Offer{},
		&Message{},
		&Dialog{},
		&Comment{},
		&UserPhones{},
		&Transaction{},
		&Account{})
	if err != nil {
		panic("migrate err: " + err.Error())
	}

	admin, err := Users.ByID(1)
	if err != nil {
		if err == errors.NotFound {
			admin = &User{
				Data: &UserData{
					FirstName:   "Уютное",
					DisplayName: "Уютное гнёздышко",
					LastName:    "Гнёздышко",
				},
				Role: User_administrator,
			}
			err = Users.Create(admin)
			if err != nil {
				panic("create admin user: " + err.Error())
			}
		} else {
			panic("get admin user: " + err.Error())
		}
	}

	err = newBloblogContext()
	if err != nil {
		panic("unitialize bloblog: " + err.Error())
	}

	TmpIndex()
}

func newBloblogContext() (err error) {
	fpath := filepath.Join(setting.App.DataDir, "data.bloblog")
	log.Printf("init bloblog in folder %s", fpath)
	blog, err = bloblog.Open(fpath)
	return
}

func Put(data []byte) (int64, error) {
	return blog.Insert(data)
}

func Get(id int64) ([]byte, error) {
	return blog.Get(id)
}

var uploadCache = map[string][]int64{}

func CachedIDs(xid string) []int64 {
	return uploadCache[xid]
}

func CacheID(xid string, id int64) {
	if arr, has := uploadCache[xid]; has {
		arr = append(arr, id)
		uploadCache[xid] = arr
	} else {
		uploadCache[xid] = []int64{id}
	}
}

func DropCache(xid string) {
	delete(uploadCache, xid)
}

var idx bleve.Index

func TmpIndex() {
	var err error
	mapping := bleve.NewIndexMapping()
	idx, err = bleve.NewMemOnly(mapping)
	if err != nil {
		panic(err)
	}
	ads, err := Offers.List()
	if err != nil {
		log.Fatalln(err)
	}
	for _, ad := range ads {
		err = idx.Index(strconv.Itoa(int(ad.Id)), ad)
		if err != nil {
			panic(err)
		}
	}
}

func Search(values url.Values) (res []Offer, err error) {
	TmpIndex()
	var queris []query.Query
	for key := range values {
		q := search.GetQuery(values, key)
		if q == nil {
			continue
		}
		queris = append(queris, q)
	}
	var dq query.Query = bleve.NewConjunctionQuery(queris...)
	if len(queris) == 0 {
		dq = bleve.NewMatchAllQuery()
	}
	req := bleve.NewSearchRequest(dq)

	resp, err := idx.Search(req)
	if err != nil {
		log.Fatalln("err search", err)
	}
	log.Printf("found %d", resp.Total)
	var ids []int64
	for _, doc := range resp.Hits {
		log.Println(doc.ID)
		id, _ := strconv.Atoi(doc.ID)
		ids = append(ids, int64(id))
	}
	log.Println(resp.Total)
	log.Printf("%v", ids)
	err = db.In("id", ids).OrderBy("id desc").Find(&res)
	return
}

func intSearch(val float64) *bleve.SearchRequest {
	min := val
	max := val
	minInclusive := true
	maxInclusive := true
	q := bleve.NewNumericRangeInclusiveQuery(&min, &max, &minInclusive, &maxInclusive)
	q.SetField("data.state")
	return bleve.NewSearchRequest(q)
}

// func SearchOnModeration() (res []Ad, err error) {
// 	req := intSearch(float64(Ad_Moderation))
// 	resp, err := idx.Search(req)
// 	if err != nil {
// 		log.Fatalln("err search", err)
// 	}
// 	log.Printf("found %d", resp.Total)
// 	var ids []int64
// 	for _, doc := range resp.Hits {
// 		log.Println(doc.ID)
// 		id, _ := strconv.Atoi(doc.ID)
// 		ids = append(ids, int64(id))
// 	}
// 	log.Println(resp.Total)
// 	log.Printf("%v", ids)
// 	return Ads.ByIDs(ids)
// }
