// Copyright 2018 Ujutnoe gnjozdyshko
//           2018 Kirill Zhuharev

package models

type ticketModel int

// Tickets public methods from here
var Tickets ticketModel

func (ticketModel) Add(userID int64, title, text string) error {
	ticket := &Ticket{
		Date:   nowDate(),
		UserId: userID,
		Title:  title,
	}
	ticketID, err := db.InsertOne(ticket)
	if err != nil {
		return err
	}

	id, err := Comments.Create(PeerType_ticket, ticketID, text)
	if err != nil {
		return err
	}

	ticket.LastCommentId = id

	_, err = db.Cols("last_comment_id").Update(ticket)
	if err != nil {
		return err
	}

	return nil
}

func (ticketModel) History(userID int64, isSupports ...bool) (res []Ticket, err error) {
	if len(isSupports) > 0 && isSupports[0] {
		err = db.Where("support_id = ?", userID).OrderBy("last_comment_id desc").Find(&res)
		return
	}
	err = db.Where("user_id = ?", userID).OrderBy("id desc").Find(&res)
	return
}
