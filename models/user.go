// Copyright 2018 Ug Ltd
//           2018 Kirill Zhuharev

package models

import (
	"github.com/golang/protobuf/ptypes"
)

func (u User) DisplayName() string {
	if u.GetData() == nil {
		return ""
	}
	return u.GetData().GetDisplayName()
}

func (u User) AvatarURL() string {
	if u.GetData() == nil {
		return ""
	}
	return u.GetData().GetAvatarUrl()
}

func NewUser() *User {
	return &User{
		Data: &UserData{
			CreatedAt: ptypes.TimestampNow(),
		},
	}
}
