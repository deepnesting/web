// Copyright 2018 Ug Ltd
//           2018 Kirill Zhuharev

package models

import (
	"fmt"
	"strconv"

	"ug/errors"
)

type userModel int

var Users userModel

func (userModel) ByID(id int64) (*User, error) {
	user := new(User)
	has, err := db.Id(id).Get(user)
	if err != nil {
		return nil, err
	}
	if !has {
		return nil, errors.NotFound
	}
	return user, nil
}

func (userModel) ByIDs(ids ...int64) (res []User, err error) {
	err = db.In("id", ids).Find(&res)
	return
}

func (userModel) ForAds(ads []Offer) (res []User, err error) {
	ids := userIDsFromAds(ads)
	err = db.In("id", ids).Find(&res)
	return
}

func (userModel) Create(u *User) (err error) {
	_, err = db.InsertOne(u)
	if err != nil {
		return err
	}
	return
}

func (userModel) Update(u *User) (err error) {
	_, err = db.Update(u)
	return
}

func (userModel) CreateByVkID(vkID int64, firstName, lastName, photo string) (*User, error) {
	user := User{
		Data: &UserData{
			FirstName:   firstName,
			LastName:    lastName,
			AvatarUrl:   photo,
			DisplayName: fmt.Sprintf("%s %s", firstName, lastName),
		},
	}
	_, err := db.InsertOne(&user)
	if err != nil {
		return nil, err
	}

	soc := &Socials{
		SocialType: SocialNetworkType_vk,
		SocialId:   fmt.Sprintf("%d", vkID),
		UserId:     user.Id,
	}
	_, err = db.InsertOne(soc)
	if err != nil {
		return nil, err
	}

	return &user, err
}

func (u userModel) ByVkID(vkID int64) (*User, error) {
	soc := new(Socials)
	has, err := db.Where("social_type = ? and social_id = ?", SocialNetworkType_vk, strconv.Itoa(int(vkID))).Get(soc)
	if err != nil {
		return nil, err
	}
	if !has {
		return nil, errors.NotFound
	}

	return u.ByID(soc.UserId)
}

func (u userModel) CreateByVkIfNotExists(vkID int64, firstName, lastName, photo string) (*User, error) {
	user, err := u.ByVkID(vkID)
	if err == nil {
		return user, nil
	}
	if err == errors.NotFound {
		return u.CreateByVkID(vkID, firstName, lastName, photo)
	}
	return nil, err
}

func (u userModel) ByPhone(phone string) (*User, error) {
	up := &UserPhones{
		UserId: 0,
		Phone:  phone,
	}
	has, err := db.Where("phone = ?", phone).Get(up)
	if err != nil {
		return nil, err
	}
	if !has {
		return nil, errors.NotFound
	}
	return u.ByID(up.UserId)
}

func (u userModel) AddPhone(userID int64, phone string) error {
	_, err := u.ByPhone(phone)
	if err != nil {
		if err != errors.NotFound {
			return err
		}
	}
	if err == nil {
		return errors.AlreadyExists
	}
	up := &UserPhones{
		UserId: userID,
		Phone:  phone,
	}
	_, err = db.InsertOne(up)
	return err
}

// MustByPhone create user if not exists
func (u userModel) MustByPhone(phone string) (*User, error) {
	user, err := u.ByPhone(phone)
	if err != nil {
		if err == errors.NotFound {
			user := NewUser()
			err = u.Create(user)
			if err != nil {
				return nil, err
			}
			return user, u.AddPhone(user.Id, phone)
		}
	}
	return user, err
}

func (userModel) List() (res []User, err error) {
	err = db.OrderBy("id desc").Find(&res)
	return
}

func (userModel) Banned() (res []User, err error) {
	err = db.Where("banned = ?", true).OrderBy("id desc").Find(&res)
	return
}

func (userModel) Ban(userID int64) error {
	_, err := db.Exec(`update "user" set banned = ? where id = ?`, true, userID)
	return err
}

func (userModel) UnBan(userID int64) error {
	_, err := db.Exec(`update "user" set banned = ? where id = ?`, false, userID)
	return err
}
