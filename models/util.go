// Copyright 2018 Ujutnoe gnjozdyshko
//           2018 Kirill Zhuharev

package models

import (
	"github.com/golang/protobuf/ptypes"
	tspb "github.com/golang/protobuf/ptypes/timestamp"
)

func nowDate() *tspb.Timestamp {
	return ptypes.TimestampNow()
}
