// Copyright 2018 Ujutnoe gnjozdyshko
//           2018 Kirill Zhuharev

package base

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"os"
	"strings"
	"time"
)

func FileExists(filename string) bool {
	_, err := os.Stat(filename)
	return err == nil
}

func FileUnmarshallJSON(filenameOrURL string, result interface{}, timeout ...time.Duration) error {
	data, err := FileGetBytes(filenameOrURL, timeout...)
	if err != nil {
		return err
	}
	return json.Unmarshal(data, result)
}

func FileGetBytes(filenameOrURL string, timeout ...time.Duration) ([]byte, error) {
	if strings.Contains(filenameOrURL, "://") {
		if strings.Index(filenameOrURL, "file://") == 0 {
			filenameOrURL = filenameOrURL[len("file://"):]
		} else {
			client := http.DefaultClient
			if len(timeout) > 0 {
				client = &http.Client{Timeout: timeout[0]}
			}
			r, err := client.Get(filenameOrURL)
			if err != nil {
				return nil, err
			}
			defer r.Body.Close()
			if r.StatusCode < 200 || r.StatusCode > 299 {
				return nil, fmt.Errorf("%d: %s", r.StatusCode, http.StatusText(r.StatusCode))
			}
			return ioutil.ReadAll(r.Body)
		}
	}
	return ioutil.ReadFile(filenameOrURL)
}
