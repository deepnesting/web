// Copyright 2018 Ug Ltd
//           2018 Kirill Zhuharev

package context

import (
	"log"
	"path/filepath"
	"time"

	"ug/models"
	"ug/pkg/base"

	"github.com/go-macaron/cache"
	"github.com/go-macaron/session"
	"github.com/hiteshmodha/goDevice"
	macaron "gopkg.in/macaron.v1"
)

type Context struct {
	*macaron.Context
	Cache   cache.Cache
	Flash   *session.Flash
	Session session.Store

	User     *models.User
	IsSigned bool
}

func Contexter() macaron.Handler {
	return func(c *macaron.Context, cache cache.Cache, sess session.Store, f *session.Flash) {
		ctx := &Context{
			Context: c,
			Cache:   cache,
			Flash:   f,
			Session: sess,
		}
		// Compute current URL for real-time change language.
		//ctx.Data["Link"] = setting.AppSubUrl + ctx.Req.URL.Path

		ctx.Data["PageStartTime"] = time.Now()
		idFace := sess.Get("u")
		if id, ok := idFace.(int64); ok && id != 0 {
			// TODO: cache this
			user, err := models.Users.ByID(id)
			if err != nil {
				log.Println("user get from db id=%d err=%s", id, err)
			}

			ctx.IsSigned = true
			ctx.User = user

			ctx.Data["IsSigned"] = ctx.IsSigned
			ctx.Data["SignedUser"] = user
		}

		ctx.Data["CurrentUrl"] = ctx.Req.URL.String()

		c.Map(ctx)
	}
}

// Autorized just hellper
func (ctx *Context) Autorized() bool {
	return ctx.User != nil
}

var tplExistsCache = map[string]bool{}

func (c *Context) HTML(status int, name string, data ...interface{}) {

	var (
		exists bool
	)

	if ex, ok := tplExistsCache[name]; ok {
		exists = ex
	} else {
		exists = base.FileExists(filepath.Join("templates", "mobile", name+".tmpl"))
		tplExistsCache[name] = exists
	}

	dt := goDevice.GetType(c.Req.Request)
	if dt == goDevice.MOBILE || dt == goDevice.TABLET {
		c.Data["IsMobile"] = true
		mobileDisabled := c.GetCookie("mobile_disabled")
		if mobileDisabled == "" && exists {
			c.HTMLSet(status, "mobile", name, data...)
			return
		}
	}

	layoutName := "layout"
	if !c.Autorized() {
		layoutName = "unauth-layout"
	}
	c.Context.HTML(status, name, c.Data, macaron.HTMLOptions{Layout: layoutName})
}
