// Copyright 2018 Ujutnoe gnjozdyshko
//           2018 Kirill Zhuharev

package search

import (
	"log"
	"net/url"
	"strconv"

	"github.com/blevesearch/bleve"
	"github.com/blevesearch/bleve/search/query"
)

// GetQuery create query if paramName exists in queryMap else return nil
func GetQuery(values url.Values, paramName string) query.Query {
	log.Printf("get query: %s", paramName)
	fn := queryMap[paramName]
	if fn != nil {
		return fn(values, paramName)
	}
	return nil
}

var queryMap = map[string]func(url.Values, string) query.Query{
	"pmin":                          newNumericRangeQuery,
	"pmax":                          newNumericRangeQuery,
	"data.comfort.balcony":          newBoolQuery,
	"data.comfort.parking_place":    newBoolQuery,
	"data.comfort.fireplace":        newBoolQuery,
	"data.comfort.loggia":           newBoolQuery,
	"data.comfort.air_conditioning": newBoolQuery,
}

func newNumericRangeQuery(values url.Values, paramName string) query.Query {
	var max, min float64
	var err error
	minStr := values.Get("pmin")
	if minStr != "" {
		min, err = strconv.ParseFloat(minStr, 64)
		log.Printf("parsed min %v, err: %s", min, err)
		if err != nil {
			return nil
		}
	}

	maxStr := values.Get("pmax")
	if maxStr != "" {
		max, err = strconv.ParseFloat(maxStr, 64)
		log.Printf("parsed max %v, err: %s", max, err)
		if err != nil {
			return nil
		}
	}

	log.Printf("max: %v, min:%v", max, min)
	var (
		rmax, rmin *float64
	)
	if max != 0.0 {
		rmax = &max
	}
	if min != 0.0 {
		rmin = &min
	}
	query := bleve.NewNumericRangeQuery(rmin, rmax)
	query.SetField("data.price")
	return query
}

func newBoolQuery(values url.Values, paramName string) query.Query {
	q := bleve.NewBoolFieldQuery(true)
	q.SetField(paramName)

	return q
}
