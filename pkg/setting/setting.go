// Copyright 2018 Ug Ltd
//           2018 Kirill Zhuharev

package setting

import (
	"io/ioutil"

	"github.com/urfave/cli"
	yaml "gopkg.in/yaml.v2"
)

var (
	confFile = "conf/app.yaml"

	//DevMode if true, run in dev mode
	DevMode bool
	// Verbose enable debug output
	Verbose bool
	// AppVer current app version
	AppVer string
	// App main config
	App struct {
		DataDir string `yaml:"data_dir"`
		// Db struct {
		// 	Driver string
		// 	Config string
		// }
	}
)

// NewContext create new context
func NewContext(ctx *cli.Context, ops ...func()) (err error) {

	for _, v := range ops {
		v()
	}

	data, err := ioutil.ReadFile(confFile)
	if err != nil {
		return
	}

	err = yaml.Unmarshal(data, &App)
	if err != nil {
		return
	}

	if dataDir := ctx.String("data_dir"); dataDir != "" {
		App.DataDir = dataDir
	}

	DevMode = ctx.Bool("dev")
	return
}

func CustomLocation(path string) func() {
	return func() {
		confFile = path
	}
}
