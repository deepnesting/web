// Copyright 2018 Ujutnoe gnjozdyshko
//           2018 Kirill Zhuharev

package smsubmit

func SendCode(receptient string) error {
	return nil
}

var allowedCodes = []string{
	"1123",
	"9987",
	"3323",
}

func CheckCode(receptient, code string) (bool, error) {
	for _, c := range allowedCodes {
		if code == c {
			return true, nil
		}
	}
	return false, nil
}
