// Copyright 2018 Ug Ltd
//           2018 Kirill Zhuharev
package upload

import (
	"fmt"

	"ug/pkg/setting"
)

func ImageURL(blobID int64) string {
	if setting.DevMode {
		return fmt.Sprintf("http://localhost:4000/images/%d.jpg", blobID)
	}
	return fmt.Sprintf("https://ugnest.com/images/%d.jpg", blobID)
}
