export GOPATH := $(PWD)

SHELL := /bin/bash
PATH := bin:$(PATH)
GOPATH := $(PWD)

PKG := `go list utekasrv/... | grep -v /vendor/`
MAIN := src/utekasrv/main.go

ifeq ($(RACE),1)
	GOFLAGS=-race
endif

VERSION?=$(shell git version > /dev/null 2>&1 && git describe --dirty=-dirty --always 2>/dev/null || echo NO_VERSION)
LDFLAGS=-ldflags "-X=main.version=$(VERSION)"

all: tools rebuild

database:
	@psql -c 'DROP SCHEMA IF EXISTS utekasrv CASCADE; CREATE SCHEMA utekasrv;' uteka
	@psql -f docs/UtekaSrv.sql uteka

tools:
	@go get -u github.com/smartystreets/goconvey
	@go get -u honnef.co/go/tools/cmd/staticcheck
	@go get -u github.com/rancher/trash
#	@go get $(PKG)

fmt:
	@gofmt -l -w -s `go list -f {{.Dir}} utekasrv/... | grep -v /vendor/`

vet:
	@go vet $(PKG)
	@bin/staticcheck $(PKG)

rebuild:
	@go build -a $(LDFLAGS) $(GOFLAGS) -o utekasrv $(MAIN)

build:
	@go build $(LDFLAGS) $(GOFLAGS) -o utekasrv $(MAIN)

clean:
	@rm -rf pkg utekasrv

run:
	@echo "Compiling"
	@go run $(LDFLAGS) $(GOFLAGS) $(MAIN) -config=cfg/local.cfg -verbose

test:
	@go test $(LDFLAGS) $(GOFLAGS) $(PKG)

test-short:
	@go test $(LDFLAGS) $(GOFLAGS) -v -test.short -test.run="Test[^D][^B]" $(PKG)

convey:
	bin/goconvey -excludedDirs "github.com,golang.org,gopkg.in,honnef.co,cloud.google.com,google.golang.org"
