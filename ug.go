package main

import (
	"html/template"
	"log"
	"os"
	"time"

	"ug/controllers/ads"
	"ug/controllers/auth"
	"ug/controllers/billing"
	"ug/controllers/comments"
	"ug/controllers/likes"
	"ug/controllers/messages"
	"ug/controllers/profile"
	"ug/controllers/search"
	"ug/controllers/uploads"
	"ug/controllers/users"
	"ug/models"
	"ug/pkg/base"
	"ug/pkg/context"
	"ug/pkg/setting"

	"github.com/go-macaron/binding"
	"github.com/go-macaron/cache"
	"github.com/go-macaron/jade"
	"github.com/go-macaron/session"
	_ "github.com/lib/pq"
	"github.com/urfave/cli"
	macaron "gopkg.in/macaron.v1"
)

var Version = "0.0.1"

func main() {
	app := &cli.App{
		Commands: cli.Commands{
			cli.Command{
				Name: "web",
				Flags: []cli.Flag{
					cli.BoolFlag{
						Name: "dev",
					},
					cli.StringFlag{
						Name: "data_dir",
					},
				},
				Action: runWeb,
			},
		},
	}
	app.Run(os.Args)
}

func runWeb(ctx *cli.Context) {
	//log.Println(res.RowsAffected)

	mode := "prod"
	if ctx.Bool("dev") {
		mode = "dev"
	}
	log.Printf("run in %s mode", mode)

	err := setting.NewContext(ctx)
	if err != nil {
		log.Fatalf("init setting err: %s", err)
	}

	models.NewContext(ctx.Bool("dev"))

	m := macaron.Classic()

	m.Use(macaron.Renderers(macaron.RenderOptions{
		Directory: "templates",
		Layout:    "layout",
		Funcs: []template.FuncMap{
			{"since": func(t time.Time) time.Duration { return time.Since(t) }},
			{"raw": func(in string) template.HTML { return template.HTML(in) }},
			{"adUser": func(ad models.Offer, users []models.User) models.User {
				for _, user := range users {
					if user.Id == ad.OwnerId {
						return user
					}
				}
				return models.User{}
			}},
			{"markdown": func(text string) template.HTML {
				return template.HTML(base.RenderMarkdownString(text))
			}},
		},
	}))

	m.Use(session.Sessioner(session.Options{
		CookieName: "s",
	}))

	m.Use(cache.Cacher())
	m.Use(context.Contexter())

	// jade
	m.Use(jade.Renderer())

	m.Get("/jade", func(r jade.Render) {
		r.HTML(200, "page", map[string]string{
			"foo": "bar",
		})
	})

	m.Get("/", func(c *macaron.Context) {
		// temp redirect for e-mail
		c.Redirect("https://ugnest.ru")
		return
		//c.HTML(200, "index", c.Data, macaron.HTMLOptions{Layout: ""})
	})

	m.Any("/reg-with-phone", auth.PhoneReg)

	m.Get("/reg", auth.Reg)

	m.Group("/auth", func() {
		m.Group("/vk", func() {
			m.Get("/", auth.Vk)
			m.Get("/callback", auth.VkCallback)
		})
		m.Group("/sms", func() {
			m.Any("/submit", auth.Submit)
		})
	})

	m.Get("/lolauth", auth.DevLogin)

	m.Get("/logout", auth.Logout)

	m.Group("/offers", func() {
		m.Get("/", ads.List)
		m.Any("/create",
			binding.BindIgnErr(models.AdForm{}),
			binding.BindIgnErr(models.OfferData{}),
			binding.BindIgnErr(models.Comfort{}),
			ads.Create)
		m.Get("/:id", ads.Show)
		m.Post("/:id/like", likes.Add)
		m.Post("/:id/comment", comments.Add)
		m.Post("/:id/publish", ads.Publish)
		m.Post("/:id/reject", ads.Reject)
	})

	m.Group("/profile", func() {
		m.Any("/personal", profile.Personal)
		m.Any("/photos-and-videos", profile.PhotosAndVideos)
	}, auth.MustSigned)

	m.Get("/status", func(c *macaron.Context) {
		c.JSON(200, "ok")
	})

	m.Group("/users", func() {
		m.Group("/:id", func() {
			m.Get("/", users.Get)
			m.Post("/ban", users.Ban)
			m.Post("/unban", users.UnBan)
		})
	})
	// m.Get("/profile", users.Get)

	m.Post("/images/upload", uploads.UploadImage)
	m.Get("/images/:id", uploads.Show)

	m.Get("/im", messages.Dialogs)
	m.Get("/im/:peer", messages.History)
	m.Post("/im/:peer", messages.Send)

	m.Get("/filter", search.Facet)

	m.Group("/lk", func() {
		m.Get("/moderation", func(ctx *macaron.Context) {
			ads, err := models.Offers.OnModeration()
			if err != nil {
				log.Println(err)
			}
			ctx.Data["ads"] = ads
			ctx.HTML(200, "lk/moderation")
		})
		m.Get("/rejected", ads.Rejected)
		m.Get("/published", ads.Approved)
		m.Get("/users", users.List)
		m.Get("/banned", users.Banned)
	})

	m.Get("/billing/refill", billing.Refill)

	m.Post("/shared/ps/1/callback", billing.YaCallback)

	m.Run()
}
